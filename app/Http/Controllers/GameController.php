<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Game;
use \DB;
use \League;

class GameController extends Controller
{
    function store(Request $request)
    {
//        dd($request);
        DB::beginTransaction();
        try {
            $game = new Game;
            $game->mdate = Date('Y-m-d H:i:s', strtotime($request->mdate));
            $game->stadium = $request->stadium;
            $game->team1 = $request->team1;
            $game->team2 = $request->team2;
            $game->save();

            DB::commit();
        } catch (\Exception $e){
            DB::rollback();
        }

        return response()->json([
            'message' => 'success'
        ]);
    }

    function index()
    {
        $games = DB::table('games')->select(DB::raw('*'))->get();
        dd($games);
        return view('list', [
            'games'=> $games
        ]);
    }
}
