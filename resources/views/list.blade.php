<html>
    <table>
        <tr>
            <td>Id</td>
            <td>Date</td>
            <td>Stadium</td>
            <td>Team 1</td>
            <td>Team 2</td>
        </tr>
        @foreach($games as $key => $game)
            <tr>
                <td>{{$game->id}}</td>
                <td>{{$game->mdate}}</td>
                <td>{{$game->stadium}}</td>
                <td>{{$game->team1}}</td>
                <td>{{$game->team2}}</td>
            </tr>
        @endforeach
    </table>
</html>

